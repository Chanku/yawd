import argparse
import http.server
import ssl
import sys

SOCKET = None
IN_REQ = False
CERT_FILE = None
KEY_FILE = None

class Special_Web_Handler(http.server.SimpleHTTPRequestHandler):
    """
    This is basically just a modified SimpleHTTPRequestHandler to handle the SSL/HTTPS stuff
    """
    def parse_request(self):
        self.command = None  # set in case of error on the first line
        self.request_version = version = self.default_request_version
        self.close_connection = True
        requestline = str(self.raw_requestline, 'iso-8859-1')
        requestline = requestline.rstrip('\r\n')
        self.requestline = requestline
        words = requestline.split()
        if len(words) == 3:
            command, path, version = words
            try:
                if version[:5] != 'HTTP/' and version[:6] != 'HTTPS/':
                    raise ValueError
                base_version_number = version.split('/', 1)[1]
                version_number = base_version_number.split(".")
                # RFC 2145 section 3.1 says there can be only one "." and
                #   - major and minor numbers MUST be treated as
                #      separate integers;
                #   - HTTP/2.4 is a lower version than HTTP/2.13, which in
                #      turn is lower than HTTP/12.3;
                #   - Leading zeros MUST be ignored by recipients.
                if len(version_number) != 2:
                    raise ValueError
                version_number = int(version_number[0]), int(version_number[1])
            except (ValueError, IndexError):
                self.send_error(
                    HTTPStatus.BAD_REQUEST,
                    "Bad request version (%r)" % version)
                return False
            if version_number >= (1, 1) and self.protocol_version >= "HTTP/1.1":
                self.close_connection = False
            if version_number >= (2, 0):
                self.send_error(
                    HTTPStatus.HTTP_VERSION_NOT_SUPPORTED,
                    "Invalid HTTP version (%s)" % base_version_number)
                return False
        elif len(words) == 2:
            command, path = words
            self.close_connection = True
            if command != 'GET':
                self.send_error(
                    HTTPStatus.BAD_REQUEST,
                    "Bad HTTP/0.9 request type (%r)" % command)
                return False
        elif not words:
            return False
        else:
            global IN_REQ # This global here is required because I had weird issues without it.
            global SOCKET # To make sure it is explicit this is a global
            global CERT_FILE
            global KEY_FILE

            # We use the IN_REQ variable to see if we are in a recursion, in order to prevent infinite recursion
            #  incase someone is using SSL and something else
            if not IN_REQ:
                IN_REQ = True
                # Uses the actual socket and wraps it in SSL. This isn't an async operation so this is safe
                SOCKET.socket = ssl.wrap_socket(SOCKET.socket,
                                                server_side=True,
                                                certfile=CERT_FILE,
                                                keyfile=KEY_FILE,
                                                ssl_version=ssl.PROTOCOL_TLSv1)

                # This actually uses the fact that SOCKET is simply an HTTPServer, which is in turn an TCPServer object
                #  therefore I call what calls this section first, this is simply a optimization.
                try:
                    SOCKET.process_request(self.request, self.client_address)
                except Exception:
                    SOCKET.handle_error(self.request, self.client_address)
                    SOCKET.shutdown_self.request(request)
                except:
                    SOCKET.shutdown_self.request(request)
                    raise
                
                #Unwraps the socket, and if it isn't wrapped, then simply ignore it.
                try:
                    SOCKET.socket = ssl.SSLSocket.unwrap(SOCKET.socket)
                except ValueError:
                    pass
                IN_REQ = False
            else:
                IN_REQ = False
                self.send_error(
                    HTTPStatus.BAD_REQUEST,
                    "Bad request syntax (%r)" % requestline)
            return False

        self.command, self.path, self.request_version = command, path, version

        # Examine the headers and look for a Connection directive.
        try:
            self.headers = http.client.parse_headers(self.rfile,
                                                     _class=self.MessageClass)
        except http.client.LineTooLong as err:
            self.send_error(
                HTTPStatus.REQUEST_HEADER_FIELDS_TOO_LARGE,
                "Line too long",
                str(err))
            return False
        except http.client.HTTPException as err:
            self.send_error(
                HTTPStatus.REQUEST_HEADER_FIELDS_TOO_LARGE,
                "Too many headers",
                str(err)
            )
            return False

        conntype = self.headers.get('Connection', "")
        if conntype.lower() == 'close':
            self.close_connection = True
        elif (conntype.lower() == 'keep-alive' and
              self.protocol_version >= "HTTP/1.1"):
            self.close_connection = False
        # Examine the headers and look for an Expect directive
        expect = self.headers.get('Expect', "")
        if (expect.lower() == "100-continue" and
                self.protocol_version >= "HTTP/1.1" and
                self.request_version >= "HTTP/1.1"):
            if not self.handle_expect_100():
                return False
        return True

def http_server(HandlerClass=http.server.SimpleHTTPRequestHandler,
                ServerClass=http.server.HTTPServer, protocol="HTTP/1.0", port=8000, bind="", use_ssl=True,
                certfile="", keyfile="", use_hackery=False):
    """This runs the HTTP server.
    It runs the server on port 8000 (or the port argument)
    """
    server_address = (bind, port)
    HandlerClass.protocol_version = protocol
    httpd = ServerClass(server_address, HandlerClass)
    if HandlerClass == Special_Web_Handler:
        global SOCKET
        SOCKET = httpd

    if use_ssl or use_hackery:
        global CERT_FILE
        global KEY_FILE

        CERT_FILE= certfile
        KEY_FILE = keyfile

    if use_ssl:
        httpd.socket = ssl.wrap_socket(httpd.socket,
                                       server_side=True,
                                       certfile=certfile,
                                       keyfile=keyfile,
                                       ssl_version=ssl.PROTOCOL_TLSv1)

    sa = httpd.socket.getsockname()
    if not use_ssl and not use_hackery:
        serve_message = "Serving HTTP on {host} port {port} (http://{host}:{port}/) ..."
    elif use_ssl:
        serve_message = "Serving HTTPS on {host} port {port} (https://{host}:{port}/) ..."
    elif use_hackery:
        serve_message = "Serving HTTP and HTTPS on {host} port {port} (http://{host}:{port}/ or https://{host}:{port}/) ..."
    print(serve_message.format(host=sa[0], port=sa[1]))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        import sys
        print("\nKeyboard interrupt received, exiting.")
        sys.exit(0)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--cgi', action='store_true',
                       help='Run as CGI Server')
    parser.add_argument('--bind', '-b', default='', metavar='ADDRESS',
                        help='Specify alternate bind address '
                             '[default: all interfaces]')
    parser.add_argument('port', action='store',
                        default=8000, type=int,
                        nargs='?',
                        help='Specify alternate port [default: 8000]')
    parser.add_argument('--use_ssl', action='store_true',
                        help='Run with SSL enabled (Not Enabled by Default)')
    parser.add_argument('--keyfile', default='',
                        help="Specify Key file if using SSL [default: '']")
    parser.add_argument('--certfile', default='',
                        help="Specify Certificate file if using SSL [default: '']")
    parser.add_argument('--use_https_and_http', '--use_hackery',  action='store_true',
                        help='Run with the hackery to allow HTTP and HTTPS connections (Not Enabled by Default)')
    args = parser.parse_args()
    if args.cgi:
        handler_class = http.server.CGIHTTPRequestHandler
    elif args.use_https_and_http:
        handler_class = Special_Web_Handler
    else:
        handler_class = http.server.SimpleHTTPRequestHandler
    if args.use_https_and_http or args.use_ssl:
        if not args.keyfile or not args.certfile:
            print("You need to specify and include the Certfile and Keyfile!")
            sys.exit(1)
    http_server(HandlerClass=handler_class, port=args.port, bind=args.bind, use_ssl=args.use_ssl,
        certfile=args.certfile, keyfile=args.keyfile, use_hackery=args.use_https_and_http)

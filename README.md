# YAWD - Yet another Web Daemon
  YAWD (Yet another Web Daemon) is a web server software that is based entirely off of the simple HTTP server bundled with Python 3 (http.server), in fact most of this code isn't mine, it's from the Python 3 Standard Library Source, I only added in my hacks.

## What is YAWD?  
As I said above, it's a simple web server that is basically the server when you run http.server in python3. 

## Features
- Everything you can do with python3 -m "http.server"
- Support for using SSL exclusively
- Support for using HTTP with HTTPS on the same server, address, and port. 

## Requirements
- Python 3.X (Python 3.4 and above recommended)

## Installation
1. Do `git pull https://bitbucket.org/Chanku/yawd`
2. Do `cd yawd/yawd/`
3. Run `python3 yawd.py` in the directory that you want to run server.pyo

## PFAQ  
#### Why Python 3?
It's not just because it's all I really know, but because of the python web server not having HTTPS support (and support for running HTTP and HTTPS on the same port and address).

#### Why not just use [INSERT WEB SERVER HERE]?
Because either:
1. I didn't know about it
2. I can't use it

#### Why do you have the license setup that you have?
Because my work technically classified as a derivative work of the cpython library http.server. Therefore I must include the license for that work (as required by the Python License) in addition to my changes (which are licensed under the MIT (Expat) License).

#### How much code did you actually write on this project?  
About 20, not counting comments, blank lines, or lines from other sections of the python library that I included and then changed to work with what I have.

## Copyright and License Information

Copyright (c) 2017 Chanku/Sapein. All rights reserved

Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017 Python Software Foundation. All rights reserved.

Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016 Python Software Foundation. All rights reserved.

Copyright (c) 2000 BeOpen.com. All rights reserved.

Copyright (c) 1995-2001 Corporation for National Research Initiatives. All rights reserved.

Copyright (c) 1991-1995 Stichting Mathematisch Centrum. All rights reserved.

See the file "LICENSE" for information on the history of this software, terms & conditions for usage, and a DISCLAIMER OF ALL WARRANTIES.

All trademarks referenced herein are property of their respective holders.
